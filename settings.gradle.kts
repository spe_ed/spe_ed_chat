rootProject.name = "Chat_Kotlin"
enableFeaturePreview("GRADLE_METADATA")

pluginManagement {
    repositories {
        gradlePluginPortal()
        jcenter()
    }
}