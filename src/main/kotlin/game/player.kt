package game

/**
 * Player
 *
 * @property x position from the head of the player
 * @property y position from the head of the player
 * @property direction of the player
 * @property speed of the player
 * @property active is a boolean whether the player is alive
 * @property name of the player
 * @constructor Create a Player
 */
data class Player(
    var x: Int,
    var y: Int,
    var direction: Direction,
    var speed: Int,
    val active: Boolean,
    val name: String = "Unknown",
)