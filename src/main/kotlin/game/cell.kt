package game

/**
 * Cell of the game board
 *
 * @property x position of the [Cell]
 * @property y position of the [Cell]
 * @property value is the state of the Cell(0 -> empty, -1 -> collision, else -> player)
 * @constructor Create Cell
 */
data class Cell(val x: Int, val y: Int, var value: Int){
    override fun toString(): String {
        return this.value.toString()
    }
}