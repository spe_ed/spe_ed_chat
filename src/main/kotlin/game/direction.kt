package game

/**
 * Direction of the player
 */
enum class Direction {
    up,
    left,
    down,
    right;

    /**
     * Left turn
     *
     * @return new direction after turned left
     */
    fun leftTurn(): Direction {
        return when (this) {
            up -> left
            left -> down
            down -> right
            right -> up
        }
    }

    /**
     * Right turn
     *
     * @return new direction after turned right
     */
    fun rightTurn(): Direction {
        return when (this) {
            up -> right
            left -> up
            down -> left
            right -> down
        }
    }


}