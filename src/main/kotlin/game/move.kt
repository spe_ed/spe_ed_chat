package game

import com.beust.klaxon.JsonObject
import com.importre.crayon.*

/**
 * Move of the player
 */
enum class Move {
    turn_left,
    turn_right,
    speed_up,
    slow_down,
    change_nothing;

    /**
     * Creates an action from a move which can be send to the server
     *
     * @return action for the server
     */
    fun toAction(): String {
        return JsonObject(hashMapOf("action" to this.name)).toJsonString()
    }

    /**
     * Paint a string in the color of the move
     *
     * @return colored text
     */
    fun paint(text: String): String {
        return when(this){
            change_nothing -> text.red()
            turn_left -> text.yellow()
            turn_right -> text.green()
            speed_up -> text.blue()
            slow_down -> text.magenta()
        }
    }
}