package game

import java.time.Instant

/**
 * Game state
 *
 * @property width of the game board
 * @property height of the game board
 * @property cells is a 2D list with the cell values of the game board(0 -> empty, -1 -> collision, else -> player)
 * @property players is a list which contains information about the players
 * @property you is your player number
 * @property running indicates whether the game is running
 * @property deadline is the date by which you must have send a move
 * @constructor Create a GameState
 */
data class GameState(
    val width: Int,
    val height: Int,
    val cells: List<List<Int>>,
    val players: Map<String, Player>,
    val you: Int,
    val running: Boolean,
    val deadline: String = ""
)

/**
 * Parsed game state
 *
 * @property round of the game
 * @constructor
 *
 * @param gameState from the current round of the game
 */
class ParsedGameState(val round: Int, gameState: GameState) {
    val width: Int = gameState.width
    val height: Int = gameState.height
    val players: Map<String, Player> = gameState.players
    val you: Int = gameState.you
    var cells: List<List<Cell>> =
        gameState.cells.mapIndexed { y, row -> row.mapIndexed { x, value -> Cell(x, y, if (value != 0) 1 else 0) } }
    val deadline: Instant = Instant.parse(gameState.deadline)
}