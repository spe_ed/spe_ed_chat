package game

import java.time.Instant

/**
 * Time from the server
 *
 * @property time from the server
 * @property milliseconds from the server
 * @constructor Create Time
 */
data class Time(
        val time: String,
        val milliseconds: Int,
) {
    /**
     * Adds time and milliseconds and parses it to an [Instant]
     *
     * @return server time as [Instant]
     */
    fun toInstant(): Instant {
        return Instant.parse(time).plusMillis(milliseconds.toLong())
    }
}