import agent.Agent
import agent.Viewer
import com.beust.klaxon.Klaxon
import com.importre.crayon.underline
import game.*
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.websocket.*
import io.ktor.client.request.*
import io.ktor.http.cio.websocket.*
import io.ktor.util.*
import kotlinx.coroutines.channels.ClosedReceiveChannelException
import kotlinx.coroutines.runBlocking

/**
 * Starts the agents connection to the websocket at the location specified by the environment variables
 */
//Websockets are still experimental
@KtorExperimentalAPI
fun main() {
    val envURL: String = System.getenv("URL") ?: "localhost"
    val envKEY: String = System.getenv("KEY")
    val envDISPLAY: Boolean = System.getenv("DISPLAY_GAME").toBoolean()
    val envMESSAGE: String = System.getenv("MESSAGE") ?: "SPEED"

    val client = HttpClient(CIO) {
        install(WebSockets)
    }

    runBlocking {
        handleWebsockets(client, envURL, envKEY, envDISPLAY, envMESSAGE)
    }
}

/**
 * Handle websockets
 *
 * @param client over which to start the connection
 * @param url to send the wss request to
 * @param time_url to get the server time
 * @param key the api key
 * @param displayImage whether to display the game state as image
 */
suspend fun handleWebsockets(client: HttpClient, url: String, key: String, displayImage: Boolean, message: String) {
    val viewer = if (displayImage) Viewer() else null
    val agent = Agent(message)
    //Connect to the specified websocket
    println("Connecting to $url")
    client.wss(
        urlString = "$url?key=$key"
    ) {
        var round = 1
        var aligned = false
        while (true) {
            // Receive websocket frame.
            // Should contain the game state information of the current round
            val frame: Frame
            try {
                frame = incoming.receive()
            } catch (exception: ClosedReceiveChannelException) {
                println(exception.message)
                break
            }

            if (frame is Frame.Text) {
                // Parse the received game state from json to the GameState class using Klaxon
                when (val gameState = Klaxon().parse<GameState>(frame.readText())) {
                    null -> {
                        // If parsing failed, stop the program
                        println("Parsing failed")
                        return@wss
                    }
                    else -> {
                        //Display the new game state if viewer has been set
                        viewer?.update(gameState.cells, gameState.you)
                        if (!gameState.running) {
                            println("Game ended")
                            return@wss
                        }

                        //Calculate move if own player is still alive
                        if (gameState.players[gameState.you.toString()]?.active == true && gameState.running) {
                            println("Round $round: ${gameState.players.filter { (_, player) -> player.active }.size} players alive".underline())

                            //To be able to write reliably,
                            // we choose first between left to right writing and up to down writing
                            // by checking the distance to the corresponding wall
                            // After aligning moves are read from the agent
                            val direction = gameState.players[gameState.you.toString()]?.direction
                            val x = gameState.players[gameState.you.toString()]?.x
                            val y = gameState.players[gameState.you.toString()]?.y
                            if (direction !== null && x !== null && y !== null) {
                                if (gameState.width - x >= gameState.height - y) {
                                    if (direction == Direction.right) {
                                        aligned = true
                                    } else if (!aligned) {
                                        println("Aligning")
                                        send(
                                            when (direction) {
                                                Direction.up -> Move.turn_right.toAction()
                                                Direction.down -> Move.turn_left.toAction()
                                                Direction.left -> Move.turn_left.toAction()
                                                else -> ""
                                            }
                                        )
                                        continue
                                    }
                                } else {
                                    if (direction == Direction.down) {
                                        aligned = true
                                    } else if (!aligned) {
                                        println("Aligning")
                                        send(
                                            when (direction) {
                                                Direction.up -> Move.turn_left.toAction()
                                                Direction.left -> Move.turn_left.toAction()
                                                Direction.right -> Move.turn_right.toAction()
                                                else -> ""
                                            }
                                        )
                                        continue
                                    }
                                }
                            }
                            //Get move from Agent
                            send(agent.makeMove())
                            round++
                        }
                    }
                }
            }
        }
    }
}
