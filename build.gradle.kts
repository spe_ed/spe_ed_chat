
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.10"
    id("com.github.johnrengelman.shadow") version "4.0.1"
    id("org.jetbrains.dokka") version "1.4.20"
    application
}
group = ""
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
    maven {
        url = uri("https://dl.bintray.com/kotlin/ktor")
    }
    maven {
        url = uri("https://dl.bintray.com/kotlin/kotlinx")
    }
    maven { url = uri("https://plugins.gradle.org/m2/") }
}
dependencies {
    testImplementation(kotlin("test-junit"))
    implementation("io.ktor:ktor-server-netty:1.4.0")
    implementation("io.ktor:ktor-html-builder:1.4.0")
    implementation("io.ktor:ktor-client-cio:1.4.0")
    implementation("org.jetbrains.kotlinx:kotlinx-html-jvm:0.7.2")
    implementation("com.beust:klaxon:5.0.1")
    implementation("com.github.jengelman.gradle.plugins:shadow:2.0.1")
    implementation("org.slf4j:slf4j-simple:1.6.1")
    implementation("se.transmode.gradle:gradle-docker:1.2")
    implementation("com.importre:crayon:0.1.0")
}
tasks.withType<KotlinCompile>() {
    logging.captureStandardOutput(LogLevel.INFO)
    kotlinOptions.jvmTarget = "11"
}
tasks.withType<Jar>() {
    manifest {
        attributes["Main-Class"] = "MainKt"
    }
    configurations["compileClasspath"].forEach { file: File ->
        from(zipTree(file.absoluteFile))
    }
}

tasks.dokkaHtml {
    dokkaSourceSets {
        configureEach { // Or source set name, for single-platform the default source sets are `main` and `test`
            // Use to include or exclude non public members
            includeNonPublic.set(true)
        }
    }
}



application {
    mainClassName = "MainKt"
}
