FROM gradle:jdk11-hotspot

ENV APP_HOME=/usr/app/
ENV ARTIFACT_NAME=Chat_Kotlin-1.0-SNAPSHOT.jar

ENV URL=wss://msoll.de/spe_ed
ENV TIME_URL=https://msoll.de/spe_ed_time
ENV KEY=keykey
ENV DISPLAY_GAME=false
ENV MESSAGE="TopRanger"

WORKDIR $APP_HOME

COPY . $APP_HOME

RUN gradle jar --no-daemon

EXPOSE 443

CMD java -jar $APP_HOME/build/libs/$ARTIFACT_NAME
