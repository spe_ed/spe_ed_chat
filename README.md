<!--
*** Build using the Best-README-Template.
-->

<!-- PROJECT LOGO -->
<br />
<p align="center">
![SPE_ED](spe_ed_logo_side.png "SPE_ED Chat")
  <h3 align="center">SPE_ED Chat Agent in Kotlin</h3>

  <p align="center">
    Implementation of an Chat for the game spe_ed.<br />
    Can write single words onto the game board <br />
    This is a project developed for the 2021 InformatiCup
    <br />
    <p>
    <a href="https://gitlab.gwdg.de/spe_ed/spe_ed_chat/issues">Report Bug</a>
    ·
    <a href="https://gitlab.gwdg.de/spe_ed/spe_ed_chat/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#built-with">Built With</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation-docker">Installation</a></li>
        <li><a href="#installation-without-docker">Installation without Docker</a></li>
      </ul>
    </li>
    <li>
      <a href="#usage">Usage</a>
    </li>
  </ol>

</details>

### Built With

<div style="display: -ms-flexbox;     display: -webkit-flex;     display: flex;     -webkit-flex-direction: row;     -ms-flex-direction: row;     flex-direction: row;     -webkit-flex-wrap: wrap;     -ms-flex-wrap: wrap;     flex-wrap: wrap;     -webkit-justify-content: space-around;     -ms-flex-pack: distribute;     justify-content: space-around;     -webkit-align-content: stretch;     -ms-flex-line-pack: stretch;     align-content: stretch;     -webkit-align-items: flex-start;     -ms-flex-align: start;     align-items: flex-start;">
<a href="https://kotlinlang.org/"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Kotlin-logo.svg/64px-Kotlin-logo.svg.png" alt="Kotlin" width="64" height="64" title="Kotlin"></a>
<a href="https://gradle.org/"><img src="https://gradle.org/images/gradle-knowledge-graph-logo.png" alt="Gradle" width="64" height="64" title="Gradle"></a>
<a href="https://www.docker.com/"><img src="https://www.docker.com/sites/default/files/d8/2019-07/vertical-logo-monochromatic.png" alt="Docker" width="64" height="64" title="Docker"></a>
</div>


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites
* [Docker](https://docs.docker.com/get-docker/)

### Installation Docker

Running this project in Docker won't produce a visual output, DISPLAY_GAME has to be false

1. Clone the repo
  ```sh
  git clone https://gitlab.gwdg.de/spe_ed/spe_ed_chat.git
  ```
2. Use Docker to build the image
  ```sh
  docker build -t spe_ed_agent .
  ```
3. Use Docker to run the image
  ```sh
  docker run -e URL="<URL>" -e KEY="<API KEY>" -e TIME_URL="<TIME URL>" -e MESSAGE="<TEXT DISPLAYED INGAME>" spe_ed_agent
  ```

  ### Installation without Docker
To use the DISPLAY_GAME option, the agent has to be run outside Docker to create a JFrame.

1. Clone the repo
  ```sh
  git clone https://gitlab.gwdg.de/spe_ed/spe_ed_chat.git
  ```
2. Set the environment variables
  ```sh
  URL=<URL>
  KEY=<API KEY>
  TIME_URL=<TIME URL>
  DISPLAY_GAME=<true / false > # Default false
  MESSAGE=<TEXT DISPLAYED INGAME>
  ```
3. Use Gradle wrapper to run the programm
  ```sh
  ./gradlew run
  ```

<!-- USAGE EXAMPLES -->
## Usage
Use the MESSAGE ENV variable to set the message to write.
Message will be written like this:

![Font](20210116-154114.jpg "Font")

Use the ENV in the Dockerfile to set custom game url if the offical servers aren't available and
 ```sh
docker-compose up --build
```
to build and start a new container with the env values overwritten

To start multiple players use
```sh
docker-compose up --scale client=<player number> --build
```
